<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>

    <?php

    $radio = 2.4;
    $perimetro = 0;
    $area = 0;

    $perimetro = 2 * pi() * $radio;
    $area = pi() * pow($radio, 2);

    echo "El radio del circulo es {$radio}<br>";
    echo "El perímetro del circulo es {$perimetro}<br>";
    echo "El área del circulo es {$area}<br>";

    ?>
</body>

</html>